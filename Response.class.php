<?php
declare(strict_types=1);
namespace Router;

class Response {

	function __construct() {
		
	}


	public function cookie(string $name, string $value, array $param = []) {
		$time = intval( time() + 60 * 60 * 24 * 30 );

		$paramDefault = [
			"expires" => $time,
			"path" => "",
			"domain" => "",
			"secure" => false,
			"httponly" => false
		];

		$opt = array_merge($paramDefault, $param);

		setcookie($name, $value, $opt['expires'], $opt['path'], $opt['domain'], $opt['secure'], $opt['httponly']);
	}


	public function clearCookie(string $name) {
		unset($_COOKIE[$name]);
		setcookie($name, "", time() - 3600, "/");
	}

}

?>
