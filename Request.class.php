<?php
declare(strict_types=1);
namespace Router;

class Request {

	public $method;
	public $protocol;
	public $hostname;
	public $query;
	public $params;
	public $originalUrl;
	public $path;

	public $cookies;
	public $secure;

	public $ip;
	public $subdomains;

	function __construct() {
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->protocol = $_SERVER['REQUEST_SCHEME'];
		$this->hostname = $_SERVER['HTTP_HOST'];
		parse_str($_SERVER['QUERY_STRING'], $this->query);
		$this->params = [];
		$this->originalUrl = $_SERVER['REQUEST_URI'];
		$this->path = "";

		$this->cookies = $_COOKIE;
		$this->secure = ('https' === $this->protocol);

		$this->ip = $this->getIp();
		$this->subdomains = $this->subdomains();
	}


	public function param(string $par): string {

		foreach ($this->params as $key => $value) {
			if ($key === $par) {
				return $value;
			}
		}

		foreach ($this->query as $key => $value) {
			if ($key === $par) {
				return $value;
			}
		}


		return "";
	}





	private function getIp(): string {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip  = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}


	private function subdomains(): array {
		$parsedUrl = parse_url($_SERVER['HTTP_HOST']);
		$host = explode('.', $parsedUrl['host']);
		$subdomains = array_slice($host, 0, count($host) - 2 );
		return $subdomains;
	}

}

?>
