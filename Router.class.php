<?php
declare(strict_types=1);
namespace Router;


class Router {

	/**
	 * @var string
	 */
	private $route;

	/**
	 * @var string
	 */
	private $method;

	/**
	 * @var string
	 */
	protected $res;

	/**
	 * @var string
	 */
	protected $req;


	/**
	 * @var function
	 */
	private $errorHandler;

	/**
	 * @var bool
	 */
	private $debug;

	/**
	 * @var bool
	 */
	private $isRoute;


	/**
	 * @var bool
	 */
	private $isError;


	/**
	 * @var bool
	 */
	private $isRun;



	function __construct(string $path = '', bool $debug = false) {
		require_once "{$path}Response.class.php";
		require_once "{$path}Request.class.php";

		$this->route = $this->getRouteFromUrl();
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->isRoute = false;
		$this->isError = false;

		$this->res = new Response();
		$this->req = new Request();

		if($debug === true) {
			$this->debug = true;
		}
	}


	function __destruct() {
		if ($this->isRoute === false && $this->isRun === false) {
			if ($this->isError === true) {
				$toRun = $this->errorHandler;
				$toRun($this->req, $this->res);
			} else {
				$this->defaultErrot($this->req, $this->res);
			}
		}

	}


	public function run() {
		if ($this->isRoute === false) {
			if ($this->isError === true) {
				$toRun = $this->errorHandler;
				$toRun($this->req, $this->res);
			} else {
				$this->defaultErrot($this->req, $this->res);
			}
		}

		$this->isRun = true;

	}


	/**
	 * @param  string   $path
	 * @param  \Closure $callback
	 * @return void
	 */
	public function all(string $path, \Closure $callback) {
		$this->action($path, 'ALL', $callback);
	}

	public function get(string $path, \Closure $callback) {
		$this->action($path, 'GET', $callback);
	}

	public function post(string $path, \Closure $callback) {
		$this->action($path, 'POST', $callback);
	}

	public function delete(string $path, \Closure $callback) {
		$this->action($path, 'DELETE', $callback);
	}

	public function put(string $path, \Closure $callback) {
		$this->action($path, 'PUT', $callback);
	}



	/**
	 * Error handler
	 * @param  \Closure $callback
	 * @return void
	 */
	public function error(\Closure $callback) {
		$this->isError = true;
		$this->errorHandler = $callback;
	}



	/**
	 * Path of page
	 * @return string
	 */
	public function path(): string {
		return $this->route;
	}



	/**
	 * Default error handler
	 * @return void
	 */
	private function defaultErrot() {
		print("<h1>404</h1>");
		exit();
	}






	/**
	 * Return url page
	 * @return string
	 */
	private function getRouteFromUrl(): string {
		$path = $_SERVER['REQUEST_URI'];

		if (strpos($path, '?') !== false) {
			$path = explode('?', $path)[0];
		}

		return $path;
	}



	/**
	 * Check coincidence of the page address with the $path
	 * @param  string   $path
	 * @param  string   $method
	 * @param  \Closure $callback
	 * @return void
	 */
	private function action(string $path, string $method, \Closure $callback) {
		$countPath = count( explode('/', $path) );
		$countRoute = count( explode('/', $this->route) );


		if ($countPath === $countRoute) {

			$pathBase = $path;
			$routeBase = $this->route;

			if (strpos($pathBase, '/:') !== false) {
				$pos = strpos($pathBase, '/:'); // The position of the first occurrence

				$routeBase = substr($this->route, 0, $pos);
				$pathBase = substr($path, 0, $pos);

				$params_str = rtrim( substr($path, $pos), '/' );
				$value_str = rtrim( substr($this->route, $pos), '/' );

				$params_array = explode('/:', $params_str);
				$value_array = explode('/', $value_str);

				$leangh = count($params_array);

				for ($i=1; $i < $leangh; $i++) {
					if ( empty($value_array[$i]) ) {
						$value_array[$i] = "";
					}

					$this->req->path = $routeBase;
					$this->req->params["{$params_array[$i]}"] = $value_array[$i];
				}
			}

			$pattern = "@" . $pathBase . "$@";

			if ( preg_match($pattern, $routeBase) && ($method === $this->method || $method === 'ALL') ) {
				$callback($this->req, $this->res);

				$this->isRoute = true;
			}

		}
	}


}



?>
